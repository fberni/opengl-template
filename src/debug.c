#include "debug.h"
#include "logger.h"
#include "utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>

void debug_callback(GLenum source, GLenum type, GLuint id,
		    GLenum severity, GLsizei length,
		    const GLchar *message, const void *userParam) {
  (void) source;
  (void) type;
  (void) id;
  (void) length;
  (void) userParam;
  // TODO: add trace severity to logger and set notification to it
  LogSeverity log_severity;
  switch (severity) {
  case GL_DEBUG_SEVERITY_HIGH		: log_severity = SEV_ERROR; break;
  case GL_DEBUG_SEVERITY_MEDIUM		: log_severity = SEV_WARNING; break;
  case GL_DEBUG_SEVERITY_LOW		: log_severity = SEV_INFO; break;
  case GL_DEBUG_SEVERITY_NOTIFICATION	: log_severity = SEV_INFO; break;
  default: UNREACHABLE;
  }
  logger_log(log_severity, "%s", message);
}
