#include "buffer.h"
#include "camera.h"
#include "debug.h"
#include "linmath.h"
#include "logger.h"
#include "utils.h"
#include "vertex_array.h"
#include "window.h"

#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#define PI 3.14159265358979323846f

static const struct
{
  vec2 pos;
  vec3 color;
} vertices[3] =
{
 { {-0.6f, -0.4f}, {1.f, 0.f, 0.f} },
 { { 0.6f, -0.4f}, {0.f, 1.f, 0.f} },
 { {  0.f,  0.6f}, {0.f, 0.f, 1.f} }
};

static const GLuint indices[3] = { 0, 1, 2 };

struct {
  WindowHandle window;

  VertexBuffer vbo;
  ElementBuffer ebo;
  VertexArray vao;

  Camera camera;
  
  struct {
    GLuint program;

    struct {
      GLint uMvp;
    } locations;
  } shader;
} gl_state = {0};

static void process_input(float delta_time) {
  if (glfwGetKey(gl_state.window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    glfwSetWindowShouldClose(gl_state.window, GLFW_TRUE);

  const float camera_speed = 1.0f * delta_time;

  if (glfwGetKey(gl_state.window, GLFW_KEY_W) == GLFW_PRESS) {
    vec3 step;
    vec3_dup(step, gl_state.camera.front);
    vec3_scale(step, step, camera_speed);
    vec3_add(gl_state.camera.position, gl_state.camera.position, step);
  }
  if (glfwGetKey(gl_state.window, GLFW_KEY_S) == GLFW_PRESS) {
    vec3 step;
    vec3_dup(step, gl_state.camera.front);
    vec3_scale(step, step, -camera_speed);
    vec3_add(gl_state.camera.position, gl_state.camera.position, step);
  }
  if (glfwGetKey(gl_state.window, GLFW_KEY_A) == GLFW_PRESS) {
    vec3 step;
    vec3_mul_cross(step, gl_state.camera.front, (vec3){0,1,0});
    vec3_norm(step, step);
    vec3_scale(step, step, -camera_speed);
    vec3_add(gl_state.camera.position, gl_state.camera.position, step);
  }
  if (glfwGetKey(gl_state.window, GLFW_KEY_D) == GLFW_PRESS) {
    vec3 step;
    vec3_mul_cross(step, gl_state.camera.front, (vec3){0,1,0});
    vec3_norm(step, step);
    vec3_scale(step, step, camera_speed);
    vec3_add(gl_state.camera.position, gl_state.camera.position, step);
  }
}

static void error_callback(int error, const char* description)
{
  (void) error;
  ERROR("%s", description);
}

GLuint create_shader(const char *filepath, GLenum type) {
  char * shader_text = read_file(filepath);
  GLuint shader = glCreateShader(type);
  glShaderSource(shader, 1, (const GLchar *const *)&shader_text, NULL);
  glCompileShader(shader);
  GLint ok;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &ok);
  if (ok != GL_TRUE) {
    GLchar message[1024];
    glGetShaderInfoLog(shader, 1024, NULL, message);
    CRITICAL("could not compile shader: %s", message);
    exit(EX_DATAERR);
  }
  free(shader_text);
  return shader;
}

GLuint create_program(GLuint vertex_shader, GLuint fragment_shader) {
  GLuint program = glCreateProgram();
  glAttachShader(program, vertex_shader);
  glAttachShader(program, fragment_shader);
  glLinkProgram(program);

  GLint ok;
  glGetProgramiv(program, GL_LINK_STATUS, &ok);
  if (ok != GL_TRUE) {
    GLchar message[1024];
    glGetProgramInfoLog(program, 1024, NULL, message);
    CRITICAL("could not link program: %s", message);
    exit(EX_DATAERR);
  }
  glDeleteShader(vertex_shader);
  glDeleteShader(fragment_shader);
  return program;
}

WindowHandle init(int width, int height, const char *window_name) {
  glfwSetErrorCallback(&error_callback);
  
  if (!glfwInit()) {
    CRITICAL("could not initialize glfw");
    exit(EX_UNAVAILABLE);
  }

  WindowHandle window = window_create(width, height, window_name);
  INFO("created window '%s' (%dx%d)", window_name, width, height);
  
  // glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  // if (glfwRawMouseMotionSupported()) {
  //   INFO("enabling raw motion for mouse");
  //   glfwSetInputMode(window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
  // }
  
  GLenum err = glewInit();
  if (err != GLEW_OK) {
    CRITICAL("could not initialize GLEW: %s", glewGetErrorString(err));
    glfwDestroyWindow(window);
    glfwTerminate();
    exit(1);
  }
  INFO("initialized GLEW");
  INFO("GL version: %s", glGetString(GL_VERSION));

  glDebugMessageCallback(&debug_callback, NULL);
  glfwSwapInterval(1);

  return window;
}

int main() {
  logger_add_sink(stderr);
  INFO("logging to stderr");

  gl_state.window = init(800, 450, "OpenGL Test");
  INFO("initialized window");

  GLuint vertex_shader = create_shader("shaders/shader.v.glsl", GL_VERTEX_SHADER);
  INFO("created vertex shader");
  GLuint fragment_shader = create_shader("shaders/shader.f.glsl", GL_FRAGMENT_SHADER);
  INFO("created fragment shader");
   
  gl_state.shader.program = create_program(vertex_shader, fragment_shader);
  INFO("created shader program");

  gl_state.shader.locations.uMvp = glGetUniformLocation(gl_state.shader.program, "uMvp");
  if (gl_state.shader.locations.uMvp < 0) {
    CRITICAL("could not retrieve uniform location");
    exit(EX_DATAERR);
  }

  gl_state.vao = vertex_array_create();
  INFO("created VAO");
  
  gl_state.vbo = vertex_buffer_create();
  vertex_buffer_set_data(&gl_state.vbo, vertices, sizeof(vertices));
  INFO("created VBO");

  gl_state.ebo = element_buffer_create();
  element_buffer_set_data(&gl_state.ebo, indices, sizeof(indices));
  INFO("created EBO");

#define LAYOUT_SIZE 2
  GLint counts[LAYOUT_SIZE] = {2, 3};
  GLenum types[LAYOUT_SIZE] = {GL_FLOAT, GL_FLOAT};
  GLuint offsets[LAYOUT_SIZE] = {0, 2 * sizeof(GLfloat)};
  BufferLayout layout = {counts, types, offsets, LAYOUT_SIZE};
  vertex_array_set_layout(&gl_state.vao, &layout);
  vertex_array_add_vbo(&gl_state.vao, &gl_state.vbo);
  vertex_array_set_ebo(&gl_state.vao, &gl_state.ebo);

  gl_state.camera = camera_create((vec3){0, 0, 3});

  double prev_time = glfwGetTime();
  while (!window_should_close(gl_state.window)) {
    double time = glfwGetTime();
    double delta_time = time - prev_time;

    process_input((float)delta_time);
    if (delta_time > 0.02) {
      WARN("timestep too long: %.3G ms", delta_time*1000);
    }
    
    int width, height;
    float ratio;
    mat4x4 m, v, p, mvp;

    glfwGetFramebufferSize(gl_state.window, &width, &height);

    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT);

    ratio = (float) width / (float) height;

    camera_get_look_at(&gl_state.camera, v);
    
    mat4x4_identity(m);
    mat4x4_perspective(p, 45.0f, ratio, 0.1f, 100.0f);
    mat4x4_mul(mvp, v, m);
    mat4x4_mul(mvp, p, mvp);

    vertex_array_bind(&gl_state.vao);
 
    glUseProgram(gl_state.shader.program);
    glUniformMatrix4fv(gl_state.shader.locations.uMvp, 1, GL_FALSE, (const GLfloat *)mvp);
    glDrawElements(GL_TRIANGLES, sizeof(indices) / sizeof(indices[0]), GL_UNSIGNED_INT, 0);
 
    glfwSwapBuffers(gl_state.window);
    glfwPollEvents();

    GLenum err;
    while ((err = glGetError()) != GL_NO_ERROR) {
      ERROR("0x%x: %s", err, gl_error_to_str(err));
    }

    prev_time = time;
  }

  window_destroy(gl_state.window);
  glfwTerminate();

  INFO("exiting");
  return 0;
}
