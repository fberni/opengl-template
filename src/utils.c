#include "utils.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>

char *read_file(const char *filepath) {
  FILE *file = fopen(filepath, "r");
  if (file == NULL) {
    CRITICAL("could not open file %s: %s", filepath, strerror(errno));
    exit(EX_NOINPUT);
  }

  if (fseek(file, 0, SEEK_END) != 0) {
    CRITICAL("could not seek end of file %s: %s", filepath, strerror(errno));
    exit(EX_IOERR);
  }

  long file_size = ftell(file);
  if (file_size < 0) {
    CRITICAL("could not tell position of file %s: %s", filepath, strerror(errno));
    exit(EX_IOERR);
  }

  char *buf = malloc((size_t)file_size + 1);
  rewind(file);
  size_t read_size = fread(buf, 1, (size_t)file_size, file);
  if (read_size != (size_t)file_size) {
    CRITICAL("could not read file %s: %s", filepath, strerror(errno));
    free(buf);
    exit(EX_IOERR);
  }
  buf[file_size] = '\0';
  fclose(file);
  return buf;
}

const char *gl_error_to_str(GLenum err) {
  switch (err) {
  case GL_NO_ERROR			: return "GL_NO_ERROR";
  case GL_INVALID_ENUM			: return "GL_INVALID_ENUM";
  case GL_INVALID_VALUE			: return "GL_INVALID_VALUE";
  case GL_INVALID_OPERATION		: return "GL_INVALID_OPERATION";
  case GL_INVALID_FRAMEBUFFER_OPERATION	: return "GL_INVALID_FRAMEBUFFER_OPERATION";
  case GL_OUT_OF_MEMORY			: return "GL_OUT_OF_MEMORY";
  case GL_STACK_UNDERFLOW		: return "GL_STACK_UNDERFLOW";
  case GL_STACK_OVERFLOW		: return "GL_STACK_OVERFLOW";
  default: UNREACHABLE;
  }
}
