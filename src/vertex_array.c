#include "vertex_array.h"

#include "utils.h"

static size_t gl_type_size(GLenum type) {
  switch (type) {
  case GL_BYTE			: return sizeof(GLbyte);
  case GL_SHORT			: return sizeof(GLshort);
  case GL_INT			: return sizeof(GLint);
  case GL_FIXED			: return sizeof(GLfixed);
  case GL_FLOAT			: return sizeof(GLfloat);
  case GL_HALF_FLOAT		: return sizeof(GLhalf);
  case GL_DOUBLE		: return sizeof(GLdouble);
  case GL_UNSIGNED_BYTE		: return sizeof(GLubyte);
  case GL_UNSIGNED_SHORT	: return sizeof(GLushort);
  case GL_UNSIGNED_INT		: return sizeof(GLuint);
  default: UNREACHABLE;
  }
}

VertexArray vertex_array_create() {
  VertexArray vao = {0};
  glCreateVertexArrays(1, &vao.handle);
  return vao;
}

void vertex_array_set_layout(VertexArray *vao, const BufferLayout *layout) {
  vao->stride = 0;
  for (size_t i = 0; i < layout->count; ++i) {
    GLuint index = (GLuint)i; // Avoid conversion warnings
    glEnableVertexArrayAttrib(vao->handle, index);
    glVertexArrayAttribBinding(vao->handle, index, 0);
    // TODO: handle normalization in layout, for now it's always
    // disabled
    glVertexArrayAttribFormat(vao->handle, index,
			      layout->counts[i], layout->types[i],
			      GL_FALSE, layout->offsets[i]);
    vao->stride += (size_t)layout->counts[i] * gl_type_size(layout->types[i]);
  }
}

void vertex_array_bind(const VertexArray *vao) {
  glBindVertexArray(vao->handle);
}

void vertex_array_add_vbo(VertexArray *vao, const VertexBuffer *vbo) {
  glVertexArrayVertexBuffer(vao->handle, 0, vbo->handle, 0, (GLsizei)vao->stride);
}

void vertex_array_set_ebo(VertexArray *vao, const ElementBuffer *ebo) {
  glVertexArrayElementBuffer(vao->handle, ebo->handle);
}
