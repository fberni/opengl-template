#include "logger.h"

#include "utils.h"

#include <assert.h>
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>

static struct {
  FILE *sinks[MAX_SINKS];
  size_t sink_count;
} log_state = {0};

static const char *logger_severity_str(LogSeverity severity) {
  switch (severity) {
  case SEV_INFO		: return "INFO   ";
  case SEV_WARNING	: return "WARNING";
  case SEV_ERROR	: return "ERROR   ";
  case SEV_CRITICAL	: return "CRITICAL";
  default: UNREACHABLE;
  }
}

bool logger_add_sink(FILE *sink) {
  if (log_state.sink_count < MAX_SINKS) {
    log_state.sinks[log_state.sink_count++] = sink;
    return true;
  }
  return false;
}

void logger_log(LogSeverity severity, const char *fmt, ...) {
  char *msg = NULL;
  va_list args;
  int size = 0;

  va_start(args, fmt);
  size = vsnprintf(msg, (size_t)size, fmt, args);
  va_end(args);

  if (size < 0) return;
  
  msg = malloc((size_t)size + 1);
  va_start(args, fmt);
  vsnprintf(msg, (size_t)size + 1, fmt, args);
  va_end(args);

  time_t rawtime = time(NULL);
  struct tm *timeinfo = localtime(&rawtime);

  char date[20] = {0};
  assert(strftime(date, sizeof(date), "%b %d %H:%M:%S", timeinfo));
  for (size_t i = 0; i < log_state.sink_count; ++i) {
    fprintf(log_state.sinks[i], "[%s] %s %s\n",
	    logger_severity_str(severity),
	    date,
	    msg);
  }
  free(msg);
}
