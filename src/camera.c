#include "camera.h"

Camera camera_create(vec3 position) {
  Camera camera;
  vec3_dup(camera.position, position);
  vec3_dup(camera.front, (vec3){0, 0, -1});
  
  return camera;
}

void camera_get_look_at(const Camera *camera, mat4x4 look_at) {
  vec3 center;
  vec3_add(center, camera->position, camera->front);
  mat4x4_look_at(look_at, camera->position, center, (vec3){0,1,0});
}
