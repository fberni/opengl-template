#include "buffer.h"

#include "logger.h"

VertexBuffer vertex_buffer_create() {
  VertexBuffer vbo;
  glCreateBuffers(1, &vbo.handle);
  return vbo;
}

void vertex_buffer_set_data(VertexBuffer *vbo, const void *data, size_t size) {
  // TODO: add support for other uses beyond GL_STATIC_DRAW
  glNamedBufferData(vbo->handle, (GLsizei)size, data, GL_STATIC_DRAW);
}

ElementBuffer element_buffer_create() {
  ElementBuffer ebo;
  glCreateBuffers(1, &ebo.handle);
  return ebo;
}

void element_buffer_set_data(ElementBuffer *ebo, const void *data, size_t size) {
  // TODO: add support for other uses beyond GL_STATIC_DRAW
  glNamedBufferData(ebo->handle, (GLsizei)size, data, GL_STATIC_DRAW);
}
