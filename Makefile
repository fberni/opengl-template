CC:=cc
CWARNS:=-Werror -Wall -Wextra -Wshadow -Wcast-align \
-Wunused -Wconversion -Wsign-conversion \
-Wmisleading-indentation -Wnull-dereference \
-Wdouble-promotion -Wformat=2

LDFLAGS:=-lm -lglfw -lGLEW -lGL

INCDIR:=include
SRCDIR:=src
BUILDDIR:=build

CFLAGS:=$(CWARNS) -std=c17 -I$(INCDIR)

# Grab all .c files as sources
SRCS:=$(wildcard $(SRCDIR)/*.c)

# Make a .o file for each .c file
OBJS:=$(patsubst $(SRCDIR)/%.c, $(BUILDDIR)/%.o, $(SRCS))

.PHONY: all clean

all: CFLAGS+=-Og -g
all: $(BUILDDIR) $(BUILDDIR)/main

clean:
	$(RM) -r $(BUILDDIR)/

# Build main from every .o file
$(BUILDDIR)/main: $(OBJS)
	$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS)

# Build a .o file from each .c file
$(BUILDDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

$(BUILDDIR):
	mkdir -p $(BUILDDIR)
