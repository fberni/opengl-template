# OpenGL Template

## Description
This is a template for writing visual applications with OpenGL.

## Features
TODO: add list of features supported by the template as they are implemented.

## Visuals

TODO: add images showcasing what can be done with this template.

## Installation
TODO: add installation steps.

## Usage
TODO: add examples

## License
This template is licensed under the MIT License. Check out the [LICENSE](LICENSE).

