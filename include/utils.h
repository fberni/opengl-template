#ifndef UTILS_H_
#define UTILS_H_

#include <GL/glew.h>

#include <stdlib.h>

#include "logger.h"

#define UNREACHABLE do {					\
    CRITICAL("%s:%d: unreachable code", __FILE__, __LINE__);	\
    abort();							\
  } while (0)

char *read_file(const char *filepath);

const char *gl_error_to_str(GLenum err);

#endif // UTILS_H_
