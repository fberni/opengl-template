#ifndef WINDOW_H_
#define WINDOW_H_

#include "options.h"

#include <stdbool.h>

struct GLFWwindow;

typedef struct GLFWwindow* WindowHandle;

WindowHandle window_create(int width, int height, const char *title);

bool window_should_close(WindowHandle window);

void window_destroy(WindowHandle window);

#endif // WINDOW_H_
