#ifndef VERTEX_ARRAY_H_
#define VERTEX_ARRAY_H_

#include <GL/glew.h>

#include "buffer.h"

typedef struct {
  GLuint handle;
  size_t stride; // TODO: this belongs to buffer, not VAO
} VertexArray;

typedef struct {
  GLint *counts;
  GLenum *types;
  GLuint *offsets;
  size_t count;
} BufferLayout;

VertexArray vertex_array_create();

void vertex_array_set_layout(VertexArray *vao, const BufferLayout *layout);

void vertex_array_bind(const VertexArray *vao);

// TODO: add support for multiple VBOs
void vertex_array_add_vbo(VertexArray *vao, const VertexBuffer *vbo);

void vertex_array_set_ebo(VertexArray *vao, const ElementBuffer *ebo);

#endif // VERTEX_ARRAY_H_
