#ifndef BUFFER_H_
#define BUFFER_H_

#include <GL/glew.h>

// ----- VERTEX BUFFER -----

typedef struct {
  GLuint handle;
} VertexBuffer;

VertexBuffer vertex_buffer_create();

void vertex_buffer_set_data(VertexBuffer *vbo, const void *data, size_t size);

// ----- ELEMENT BUFFER -----

typedef struct {
  GLuint handle;
} ElementBuffer;

ElementBuffer element_buffer_create();

void element_buffer_set_data(ElementBuffer *ebo, const void *data, size_t size);

#endif // BUFFER_H_
