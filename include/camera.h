#ifndef CAMERA_H_
#define CAMERA_H_

#include "linmath.h"

typedef struct {
  vec3 position;
  vec3 front;
} Camera;

Camera camera_create(vec3 position);

void camera_get_look_at(const Camera *camera, mat4x4 look_at);

#endif // CAMERA_H_
