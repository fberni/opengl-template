#ifndef DEBUG_H_
#define DEBUG_H_

#include <GL/glew.h>

void debug_callback(GLenum source, GLenum type, GLuint id,
		    GLenum severity, GLsizei length,
		    const GLchar *message, const void *userParam);
#endif // DEBUG_H_
