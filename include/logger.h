#ifndef LOG_H_
#define LOG_H_

#include <stdbool.h>
#include <stdio.h>

#define CRITICAL(...) logger_log(SEV_CRITICAL, __VA_ARGS__)
#define ERROR(...) logger_log(SEV_ERROR, __VA_ARGS__)
#define WARN(...) logger_log(SEV_WARNING, __VA_ARGS__)
#define INFO(...) logger_log(SEV_INFO, __VA_ARGS__)

#define MAX_SINKS 16

typedef enum {
	      SEV_INFO,
	      SEV_WARNING,
	      SEV_ERROR,
	      SEV_CRITICAL,
	      NUM_SEV,
} LogSeverity;

bool logger_add_sink(FILE *sink);

void logger_log(LogSeverity severity, const char *fmt, ...);

#endif // LOG_H_
